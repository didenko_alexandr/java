package ru.dav.book;

public class Demo {
    public static void main(String[] args) {

        Book book1 = new Book("Пушкин", "Медный всадник", 2011);

        Book book2 = new Book("Есенин", "Черный человек", 1989);

        Book book3 = new Book("Толстой", "Анна Каренина", 2018);

        Book book4 = new Book("Гоголь", "Мёртвые души", 1931);

        Book book5 = new Book("Тургенев", "Отцы и дети", 2019);

        Book[] books = {book1, book2, book3, book4, book5};

        yearsBook2018(books);

        sameYears(book1, book2);

    }

    private static void yearsBook2018(Book[] books) {

        for (Book book : books) {

            if (book.getYear() == 2018) {

                System.out.println(book);

            }

        }

    }

    private static void sameYears(Book book1, Book book2) {

        if (book1.isSameYears(book2)) {

            System.out.println("Книга выпущенна в 2018 году");

        } else {

            System.out.println("Книга не выпущенна в 2018 году");

        }

    }

}
