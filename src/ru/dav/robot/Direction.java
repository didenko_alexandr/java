package ru.dav.robot;

public enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT
}