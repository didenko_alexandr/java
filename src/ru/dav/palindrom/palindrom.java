package ru.dav.palindrom;

import java.util.Scanner;

public class palindrom {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Введите фразу: ");
        String s = scanner.nextLine();
        s = s.replaceAll("[^A-Za-zА-Яа-я0-9]", "");
        if (s.toLowerCase().equals((new StringBuffer(s)).reverse().toString().toLowerCase())) {
            System.out.println("Это палиндром" );
        } else {
            System.out.println("Это не палиндром");
        }
    }
}
