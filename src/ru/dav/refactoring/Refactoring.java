package ru.dav.refactoring;

import java.util.Scanner;

class Refactoring {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число :");
        double d = scanner.nextDouble();
        String sResult = "";
        long numberBits = Double.doubleToLongBits(d);

        sResult = Long.toBinaryString(numberBits);
        System.out.println("Представление вещественного числа в формате чисел с плавающей точкой");

        System.out.format("Число: %10.15f\n", d);
        System.out.println("Формат чисел с плавающей точкой:");

        //ведущий ноль заботливо сокращен системой, поэтому его нужно восстановить
        System.out.println(d > 0 ? "0" + sResult : sResult);
    }
}
