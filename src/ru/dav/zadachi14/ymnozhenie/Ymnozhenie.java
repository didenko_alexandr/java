package ru.dav.zadachi14.ymnozhenie;

import java.util.Scanner;

public class Ymnozhenie {
    private static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Введите число, которое нужно умножить:");
        int m = sc.nextInt();
        for (int i = 1; i < 10; i++) {
            System.out.println(i + " * " + m + " = " + i * m);
        }
    }
}
