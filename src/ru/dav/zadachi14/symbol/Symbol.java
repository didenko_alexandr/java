package ru.dav.zadachi14.symbol;

import java.util.Scanner;

public class Symbol {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите символ:");
        String symbol = sc.nextLine();
        char c = symbol.charAt(0);
        if (Character.isDigit(c)) {
            System.out.println("Это цифра!");
        }
        if (Character.isLetter(c)) {
            System.out.println("Это буква!");
        }
        if (".,:;".contains(symbol)) {
            System.out.println("Это знак пунктуации");
        }

    }
}
