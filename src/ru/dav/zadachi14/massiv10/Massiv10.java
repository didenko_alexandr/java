package ru.dav.zadachi14.massiv10;

public class Massiv10 {
    public static void main(String[] args) {

    int[] a = new int[] {100,200,300,400,500};

        for (int i=0; i<a.length; i++) {
            System.out.print(i + " - ");
            System.out.println(a[i]);
        }

        System.out.println("Увеличение массива на 10%");

        for (int k=0; k<a.length;k++) {
            a[k]=(int) (a[k]+a[k]*0.10);
            System.out.print(k + " - ");
            System.out.println(a[k]);
        }
    }

}
