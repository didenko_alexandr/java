package ru.dav.zadachi14.doubleNumber;

import java.util.Scanner;

public class DoubleNumber {
    private static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Введите число:");
        double number = sc.nextDouble();
        if(number%1==0) {
            System.out.println("Число является целым!");
        }
        else {
            System.out.println("Число не является целым!");
        }
    }
}
