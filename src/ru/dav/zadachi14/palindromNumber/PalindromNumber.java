package ru.dav.zadachi14.palindromNumber;

import java.util.Scanner;

public class PalindromNumber {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число: ");
        String s = sc.nextLine();
        s = s.replaceAll("[0-9]", "");
        if (s.toLowerCase().equals((new StringBuffer(s)).reverse().toString().toLowerCase()))
        {
            System.out.println("Число является палиндромом!" );
        }
        else System.out.println("Число не является палиндромом");
    }
}
