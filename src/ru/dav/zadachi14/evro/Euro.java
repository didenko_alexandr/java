package ru.dav.zadachi14.evro;

import java.util.Scanner;

/**
 *  Класс, позволяющий нам переводить из рублей в евро
 *
 * @author Didenko Alexander 18it18
 */

public class Euro {
    private static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Введите количество рублей:");
        double ruble = sc.nextDouble();
        System.out.println("Введите курс евро:");
        double course = sc.nextDouble();

        System.out.println(translationOfCourse(ruble, course));
    }

    /**
     * Метод перевода рублей в евро
     * @param ruble рубли
     * @param course курс евро
     * @return выводит евро
     */
    private static double translationOfCourse(double ruble, double course){
        double euro = ruble/course;
        return euro;
    }
}
