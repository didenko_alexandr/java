package ru.dav.zadachi14.palindrom;

import java.util.Scanner;

public class Palindrom {
    private static Scanner sc = new Scanner(System.in);
        public static void main(String[] args) {

            System.out.println("Введите фразу: ");
                String s = sc.nextLine();
                s = s.replaceAll("[^A-Za-zА-Яа-я0-9]", "");
                if (s.toLowerCase().equals((new StringBuffer(s)).reverse().toString().toLowerCase()))
                {
                    System.out.println("Это палиндром!" );
                }
                else System.out.println("Это не палиндром");
    }
}
