package ru.dav.zadachi14.grom;

import java.util.Scanner;

public class Grom {
    private static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        double kmCh = 1234.8;
        double mS = (1234.8 * 1000) / 3600;
        System.out.println("Введите промежуток времени между вспышкой молнии и звуком (в секундах):");
        double t = sc.nextDouble();
        double s = mS * t;
        System.out.println("Расстояние = " + s + "m");
    }
}
