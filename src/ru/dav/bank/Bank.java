package ru.dav.bank;

public class Bank {
    private double balance;
    private double percent;

    public Bank(double balance, double percent) {
        this.balance = balance;
        this.percent = percent;
    }

    public double getBalance() {
        return balance;
    }

    public double getPercent() {
        return percent;
    }

    @Override
    public String toString() {
        return "Банковский счет(" +
                "баланс = " + balance + "руб." +
                ", процентная ставка = " + percent + "%" +
                ')';
    }
}
