package ru.dav.college;

public class Student extends Person {
    private int year;
    private String code;

    Student(String surname, Gender gender, int year, String code) {
        super(surname, gender);
        this.year = year;
        this.code = code;
    }

    int getYear() {
        return year;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "Суденты (" +
                "фамилия - " + getSurname() +
                ", пол - " + getGender() +
                ", год поступления - " + year +
                ", код специальности - " + code +
                ')';
    }
}
