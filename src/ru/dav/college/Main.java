package ru.dav.college;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Student s1 = new Student("Белова", Gender.ЖЕНСКИЙ, 2017, "09.02.07");
        Student s2 = new Student("Иванова", Gender.ЖЕНСКИЙ, 2018, "05.05.07");
        Student s3 = new Student("Глазунов", Gender.МУЖСКОЙ, 2019, "04.01.06");
        Student s4 = new Student("Тишева", Gender.ЖЕНСКИЙ, 2017, "07.02.08");
        Student s5 = new Student("Грознов", Gender.МУЖСКОЙ, 2016, "08.03.07");

        ArrayList<Student> students = new ArrayList<>();

        students.add(s1);
        students.add(s2);
        students.add(s3);
        students.add(s4);
        students.add(s5);

        Teacher t1 = new Teacher("Тарханов", Gender.МУЖСКОЙ, "Информатика", true);
        Teacher t2 = new Teacher("Титишев", Gender.МУЖСКОЙ, "Русский", true);
        Teacher t3 = new Teacher("Ивашкина", Gender.ЖЕНСКИЙ, "Биология", false);
        Teacher t4 = new Teacher("Гришина", Gender.ЖЕНСКИЙ, "Информатика", true);
        Teacher t5 = new Teacher("Тарханова", Gender.ЖЕНСКИЙ, "История", false);

        ArrayList<Teacher> teachers = new ArrayList<>();

        teachers.add(t1);
        teachers.add(t2);
        teachers.add(t3);
        teachers.add(t4);
        teachers.add(t5);

        ArrayList<Person> persons = new ArrayList<>();

        input(teachers, students, persons);

        infOfPersons(persons);

        System.out.println("Кол-во девушек, поступивших в 2017 г.: " + infOfStudents(students));

        infOfTeachers(teachers);


    }

    private static ArrayList<Person> input(ArrayList<Teacher> teachers, ArrayList<Student> students,ArrayList<Person> personArrayList) {
        System.out.println("Cколько человек вы хотите ввести?");
        int num = sc.nextInt();
        for (int i = 0; i < num; i++) {
            System.out.println("Кого вы хотите занести в список?");
            if (sc.nextLine().equals("ученик")) {
                students.add(inputStudent());
            }
            if (sc.nextLine().equals("учитель")) {
                teachers.add(inputTeacher());
            }
        }
        for (int z = 0; z < students.size(); z++) {
            personArrayList.add(students.get(z));
        }
        for (int x = 0; x < teachers.size(); x++) {
            personArrayList.add(teachers.get(x));
        }
        return personArrayList;
    }

    private static Student inputStudent() {
        Gender gender = Gender.МУЖСКОЙ;
        System.out.println("Введите год поступления: ");
        int years = sc.nextInt();
        System.out.print("Введите специальность: ");
        sc.nextLine();
        String code = sc.nextLine();
        System.out.print("Введите фамилию: ");
        String surname = sc.nextLine();
        System.out.print("Введите пол(1 - Мужской, 2 - Женский): ");
        String gend = sc.nextLine();
        if (gend.equals("1")) {
            gender = Gender.МУЖСКОЙ;
        }
        if (gend.equals("2")) {
            gender = Gender.ЖЕНСКИЙ;
        }
        return new Student (surname, gender, years, code);
    }

    private static Teacher inputTeacher() {
        Gender gender = Gender.МУЖСКОЙ;
        System.out.println("Введите дисциплину: ");
        String discipline = sc.nextLine();
        System.out.print("Введите кураторство: ");
        boolean curator = sc.nextBoolean();
        System.out.print("Введите фамилию: ");
        sc.nextLine();
        String surname = sc.nextLine();
        System.out.print("Введите пол(1 - Мужской, 2 - Женский): ");
        String gend = sc.nextLine();
        if (gend.equals("1")) {
            gender = Gender.МУЖСКОЙ;
        }
        if (gend.equals("2")) {
            gender = Gender.ЖЕНСКИЙ;
        }
        return new Teacher (surname, gender, discipline, curator);
    }

    static int infOfStudents(ArrayList<Student> students) {
        int x = 0;
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getYear() == 2017 && students.get(i).getGender() == Gender.ЖЕНСКИЙ) {
                x++;
            }
        }
        return x;
    }

    private static void infOfTeachers(ArrayList<Teacher> teachers) {
        for (int i = 0; i < teachers.size(); i++) {
            if (teachers.get(i).isCurator()) {
                System.out.println(teachers.get(i).getSurname() + " является куратором.");
            }
        }
    }

    private static void infOfPersons(ArrayList<Person> persons) {
        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getGender() == Gender.МУЖСКОЙ) {
                System.out.println(persons.get(i).getSurname() + " - пол - Мужской.");
            }
        }
    }
}
