package ru.dav.college;

class Person {
    private String surname;
    private Gender gender;

    Person(String surname, Gender gender) {
        this.gender = gender;
        this.surname = surname;
    }

    String getSurname() {
        return surname;
    }

    Gender getGender() {
        return gender;
    }
}
