package ru.dav.college;

public class Teacher extends Person{
    private String discipline;
    private boolean curator;

    Teacher(String surname, Gender gender, String discipline, boolean curator) {
        super(surname, gender);
        this.discipline = discipline;
        this.curator = curator;
    }

    public String getDiscipline() {
        return discipline;
    }

    boolean isCurator() {
        return curator;
    }

    @Override
    public String toString() {
        return "Преподователи (" +
                "фамилия - " + getSurname() +
                ", пол - " + getGender() +
                ", дисциплина - " + discipline +
                ", куратор - " + curator +
                ')';
    }
}
