package ru.dav.calc;

public class MathInt {
    public static int add(int n1, int n2) {
        int result = n1 + n2;
        return result;
    }

    public static int sub(int n1, int n2) {
        int result = n1 - n2;
        return result;
    }

    public static int div(int n1, int n2) {
        int result = n1 / n2;
        return result;
    }

    public static int mult(int n1, int n2) {
        int result = n1 * n2;
        return result;
    }

    public static int exp(int n1, int n2) {
        int result = (int) Math.pow(n1, n2);
        return result;
    }

    public static int rem(int n1, int n2) {
        int result = n1 % n2;
        return result;
    }
}
