package ru.dav.calc;

import ru.dav.calc.MathInt;
import java.util.Scanner;
    public class calculator1 {
        public static void main(String[] args) {

            String operations;
            Scanner sc = new Scanner(System.in);
            System.out.println("Первое число: ");
            int n1 = sc.nextInt();
            System.out.println("Второе число: ");
            int n2 = sc.nextInt();
            Scanner oper = new Scanner(System.in);
            System.out.println("Введите выполняемую операцию: ");
            operations = oper.next();
            int result = 0;
            switch (operations) {
                case "+":
                    result = MathInt.add(n1,n2);
                    break;
                case "-":
                    result = MathInt.sub(n1,n2);
                    break;
                case "/":
                    result = MathInt.div(n1,n2);
                    break;

                case "*":
                    result = MathInt.mult(n1,n2);
                    break;

                case "^":
                    result = MathInt.exp(n1,n2);
                    break;

                case "%":
                    result = MathInt.rem(n1,n2);
                    break;
            }
            System.out.printf("Результат: \n%d",result);
        }
    }