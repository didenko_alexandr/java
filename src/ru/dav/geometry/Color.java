package ru.dav.geometry;

public enum Color {
    RED,
    BLUE,
    YELLOW,
    PURPLE,
    GREEN,
    BLACK,
    WHITE
}
