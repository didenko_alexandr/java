package ru.dav.geometry;

public class Demo {
    public static void main(String[] args) {
        Circle circle = new Circle(Color.BLACK, new Point(), 5);
        Triangles triangle = new Triangles(Color.BLUE, new Point(0, 0), new Point(1, 0), new Point(0, 1));
        Square square = new Square(Color.RED, 5, new Point());

        Shape[] shapes = {circle, triangle, square};
        printArray(shapes);

        System.out.println();

        Shape maxShape = maxShapeArea(shapes);
        System.out.println("Max area: "+maxShape);

    }

    private static Shape maxShapeArea(Shape[] shapes) {
        int sizeShapes = shapes.length;
        double max = 0;
        int part = 0;
        for (int i = 0; i < sizeShapes; i++) {
            double area = shapes[i].area();
            if (max < area) {
                max = area;
                part = i;
            }
        }
        return shapes[part];
    }

    private static void printArray(Shape[] shapes) {
        int sizeShapes = shapes.length;
        System.out.println("Массив фигур: ");
        for (int i = 0; i < sizeShapes; i++) {
            System.out.println(shapes[i] + " ");
        }
    }
}
