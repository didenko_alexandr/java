package ru.dav.geometry;

public class Square extends Shape {
    private double side;
    private Point corner; // corner - угол


    public Square(Color color, double side, Point corner) {
        super(color);
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public Point getCorner() {
        return corner;
    }

    @Override
    public String toString() {
        return "Кадрат{" +
                "Сторона =" + side +
                ", Угол=" + corner +
                '}';
    }

    @Override
    public double area() {
        return side * side;
    }
}