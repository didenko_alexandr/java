package ru.dav.country;

import java.util.ArrayList;
import java.util.Scanner;

public class Demo {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        Country usa = new Country("США", "Вашингтон", 10000000, 327);
        Country russia = new Country("Россия", "Москва", 17000000, 145);
        Country greatBritain = new Country("Великобритания", "Лондон", 245000, 66);
        Country japan = new Country("Япония", "Токио", 370000, 126);

        System.out.println("Введите количество стран: ");
        int num = scanner.nextInt();
        ArrayList<Country> inputCountry = new ArrayList<>();

        for (int i = 0; i < num; i++); {
            inputCountry.add(input());
        }

        inputCountry.add(russia);
        inputCountry.add(usa);
        inputCountry.add(greatBritain);
        inputCountry.add(japan);
        witchTheBiggestCountry(inputCountry);
    }

    private static Country input() {
        System.out.printf("Введите название: ");
        scanner.nextLine();
        String name = scanner.nextLine();
        System.out.printf("Введите столицу: ");
        String capital = scanner.nextLine();
        System.out.printf("Введите площадь: ");
        double square = scanner.nextDouble();
        System.out.printf("Введите население: ");
        int population  =scanner.nextInt();
        return new Country(name,capital,square,population);
    }

    private static void witchTheBiggestCountry (ArrayList <Country> inputCountry) {
        double max = 0;
        int part = 0;

        for (int i = 0; i < inputCountry.size(); i++) {
            double density = inputCountry.get(i).density();
            if (max < density) {
                part = i;
                max = density;
            }
        }
        System.out.println("Страна с самой большой плотностью населения: " + inputCountry.get(part));
    }
}