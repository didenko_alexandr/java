package ru.dav.country;

public class Country {
    private String name;
    private String capital;
    private double square;
    private int population;

    public Country(String name, String capital, double square, int population) {
        this.name = name;
        this.capital = capital;
        this.square = square;
        this.population = population;
    }

    Country() {
        this("Неизвeстно", "Неизвестно", 0, 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public double getSquare() {
        return square;
    }

    public void setSquare(double square) {
        this.square = square;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    @Override
    public String toString() {
        return "Название - " + name +
                ", Столица - " + capital +
                ", Площадь = " + square +
                ", Популяция = " + population;
    }
    public double density() {
        return population / square;
    }
}

