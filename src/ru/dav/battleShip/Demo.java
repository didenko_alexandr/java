package ru.dav.battleShip;

import java.util.Scanner;

public class Demo {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int lengthGateway = (int) (5 + Math.random() * 15);
        int lengthShip = (int) (1 + Math.random() * 4);
        int start = (int) (1 + Math.random() * lengthGateway - lengthShip);
        System.out.println("Длинна шлюза - " + (lengthGateway));
        Ship ship = new Ship(start, lengthShip);
        shots(ship);
    }

    private static void shots(Ship ship) {
        int x = 0;
        int shot;
        do {
            System.out.println("Введите координату стрельбы: ");
            shot = scanner.nextInt();
            System.out.println(ship.shot(shot));
            x++;
        } while (!ship.shot(shot).equals("Убит"));
        System.out.println("Для того, чтобы потопить корабль, вам понадобилось: " + x + " попытки");
    }
}
