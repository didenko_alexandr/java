package ru.dav.battleShip;

import java.util.ArrayList;

class Ship {
    private ArrayList<Integer> location = new ArrayList<>();

    Ship(int start, int lengthShip) {
        for (int i = start; i < start + lengthShip; i++) {
            location.add(i);
        }
    }

    String shot(int shot) {
        String message = "Промах";
        if (location.contains(shot)) {
            location.remove((Integer) shot);
            message = "Попал";
        }
        if (location.isEmpty()) {
            message = "Убит!";
        }
        return message;
    }

    @Override
    public String toString() {
        return "Корабль (" + location + ')';
    }

}