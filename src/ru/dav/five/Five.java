package ru.dav.five;

import java.util.Scanner;

public class Five {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int a;
        int b;
        int five = 0;
        System.out.println("Введите начало диапазона: ");
        a = sc.nextInt();
        System.out.println("Введите конец диапазона: ");
        b = sc.nextInt();
        for (int i = a; i < b; i++) {
            if (i % 5 == 0) {
                five++;
            }
        }
        System.out.println("Кол-во чисел кратных 5: " + five);
    }
}