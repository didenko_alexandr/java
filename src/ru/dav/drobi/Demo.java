package ru.dav.drobi;

import java.util.Scanner;


public abstract class Demo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Rational result;
        System.out.println("Введите выражение: ");
        String phr = sc.nextLine();
        String[] array = phr.split(" ");
        String[] number1 =(array[0]).split("/");
        int num1 = Integer.parseInt(number1[0]);
        int den1 = Integer.parseInt(number1[1]);
        Rational r1 = new Rational(num1, den1);
        String phrase = array[1];
        String[] number2 = String.valueOf(array[2]).split("/");
        int num2 = Integer.parseInt(number2[0]);
        int den2 = Integer.parseInt(number2[1]);
        Rational r2 = new Rational(num2, den2);
        result = operation(r1, r2, phrase);
        System.out.println(result.simplify());
    }


    private static Rational operation(Rational r1, Rational r2, String phr) {
        Rational r3 = new Rational();
        switch (phr) {
            case "+":
                r3 = r1.add(r2);
                break;

            case "-":
                r3 = r1.sub(r2);
                break;

            case "/":
                r3 = r1.div(r2);
                break;

            case "*":
                r3 = r1.mul(r2);
                break;

            default:
                System.out.println("Знак операции не верен :(");

        }
        return r3;
    }
}