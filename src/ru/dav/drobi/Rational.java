package ru.dav.drobi;

@SuppressWarnings("unused")
public class Rational {
    private int num;
    private int den;

    Rational(int num, int den) {
        this.num = num;
        this.den = den;
        simplify();
    }

    Rational() {
        this(1, 1);
    }

    private int getNum() {
        return num;
    }

    private int getDen() {
        return den;
    }

    Rational add(final Rational rat) {
        return new Rational(num * rat.den + den * rat.num, den * rat.den).simplify();
    }

    Rational sub(final Rational rat) {
        return new Rational(num * rat.den - den * rat.num, den * rat.den).simplify();
    }

    Rational div(final Rational rat) {
        return new Rational(num * rat.num, den * rat.den).simplify();
    }


    Rational mul(final Rational rat) {
        return new Rational(num * rat.den, den * rat.num).simplify();
    }


    public String toString() {
        return num +
                "/" + den;
    }

    public static int nod(int num, int den) {
        while (num != den) {
            if (num > den) {
                num = num - den;

            } else {
                den = den - num;
            }
        }
        return num;
    }

    Rational simplify() {
        long limit = Math.min(num, den);

        for (long i = 2; i <= limit; i++) {
            if (num % i == 0 && den % i == 0) {
                num /= i;
                den /= i;
            }
        }
        return this;
    }
}
