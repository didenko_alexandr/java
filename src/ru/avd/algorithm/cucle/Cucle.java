package ru.avd.algorithm.cucle;

import java.util.Scanner;

public class Cucle {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        int[] a = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println("Введите значение для поиска: ");
        int z = sc.nextInt();

        int x = searchingForAnItem(a,z);
        if (x != -1){
            System.out.println("Элемент находится под индексом - " + searchingForAnItem(a,z));
        }
        if (x == -1) {
            System.out.println("Такого элемента в массиве нет!");
        }
    }

    private static int searchingForAnItem(int[] a, int z){
        for (int i = 0; i < a.length; i++) {
            if (a[i] == z) {
                return i+1;
            }
        }
        return -1;
    }
}
