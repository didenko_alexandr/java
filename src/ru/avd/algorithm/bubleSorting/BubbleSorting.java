package ru.avd.algorithm.bubleSorting;

import java.util.Arrays;

public class BubbleSorting {
    public static void main(String[] args) {
        int[] arr = {9, 8, 3, 6, 7, 5, 1, 2, 4};
        bubbleSort(arr);
    }

    private static void bubbleSort(int[] arr) {

        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {

                if (arr[j] > arr[j + 1]) {
                    int replace = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = replace;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }
}
