package ru.avd.algorithm.CucleArrayList;

import java.util.ArrayList;
import java.util.Scanner;

public class CucleArrayList {

    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<> ();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);

        System.out.println("Введите значение для поиска: ");
        int index = list.indexOf(sc.nextInt());

        int x = searchingForAnItem(index);
        if (x != -1){
            System.out.println("Элемент находится под индексом - " + searchingForAnItem(index));
        }
        if (x == -1) {
            System.out.println("Такого элемента в массиве нет!");
        }
    }

    private static int searchingForAnItem(int index){
        if(index > 9){
            return -1;
        }
        return index;
    }
}
